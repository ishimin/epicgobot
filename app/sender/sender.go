package sender

import (
	"github.com/rs/zerolog/log"

	"epicbot/app"
	"epicbot/app/db"
	"epicbot/app/keyboards"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

func SendMessageEntity(
	bot *tgbotapi.BotAPI,
	s *app.State,
	db db.Conn,
	adminMode bool,
	chatID,
	userID int64,
	userName string,
	m tgbotapi.MessageConfig,
	displayMenu bool) {
	role, err := db.UserGetOrAdd(userID, userName)
	if err != nil {
		log.Err(err).
			Int64("userID", userID).
			Str("userName", userName).
			Msg("can't get or update user")
	}

	if displayMenu == true {
		m.ReplyMarkup = keyboards.UserKeyboard
		if role == app.RoleAdmin {
			m.ReplyMarkup = keyboards.AdminKeyboard
			if s.CurrentState == app.RunBroadcast {
				m.ReplyMarkup = keyboards.RunBroadcastKeyboard
			}
		}
		if adminMode {
			m.ReplyMarkup = keyboards.AdminModeKeyboard
		}
	}

	if _, err := bot.Send(m); err != nil {
		log.Err(err).Msg("bot.SendMessageEntity")
	}
}

func SendMessage(
	bot *tgbotapi.BotAPI,
	s *app.State,
	db db.Conn,
	adminMode bool,
	chatID,
	userID int64,
	userName string,
	text string,
	displayMenu bool) {
	role, err := db.UserGetOrAdd(userID, userName)
	if err != nil {
		log.Err(err).
			Int64("userID", userID).
			Str("userName", userName).
			Msg("can't get or update user")
	}

	msg := tgbotapi.NewMessage(chatID, text)
	if displayMenu == true {
		msg.Text = s.TemplateText + "\n" + text
		msg.ReplyMarkup = keyboards.UserKeyboard
		if role == app.RoleAdmin {
			msg.ReplyMarkup = keyboards.AdminKeyboard
		}
		if adminMode {
			msg.ReplyMarkup = keyboards.AdminModeKeyboard
		}
	}

	if _, err := bot.Send(msg); err != nil {
		log.Err(err).Msg("bot.SendMessage")
	}
}

func SendPopup(bot *tgbotapi.BotAPI, queryID string, userName string, text string) {
	// Respond to the callback query
	callback := tgbotapi.NewCallback(queryID, text)
	if _, err := bot.Request(callback); err != nil {
		log.Err(err).Msg("bot.Request")
	}
}
