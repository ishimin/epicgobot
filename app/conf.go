package app

type Config struct {
	TelegramBaseURL   string
	TelegramToken     string
	Host              string
	Database          string
	User              string
	Password          string
	Port              int
	CloudBaseURL      string
	PhotoHostDuration int
	CloudKey          string
	BotDebug          bool
	BotTimeout        int
}

func (c *Config) LoadDefaults() {
	// @betplus_bot
	c.TelegramToken = "1861610990:AAHWE0cuZKChmUgef8nrbwiy7nnlD6EpoQs"
	c.TelegramBaseURL = "https://api.telegram.org/bot"
	c.Host = "94.250.255.56"
	c.Database = "bottgdb"
	c.User = "postgres"
	c.Password = "pgAdmin"
	c.Port = 5432
	c.CloudBaseURL = "https://api.imgbb.com/1/upload"
	c.PhotoHostDuration = 172800
	c.CloudKey = "c9f0a4b274d2081571c1794d567256bd"
	c.BotDebug = false
	c.BotTimeout = 60
}
