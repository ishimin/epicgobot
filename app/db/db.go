package db

import (
	"errors"
	"fmt"
	"time"

	"epicbot/app"

	"github.com/jackc/pgx"
)

type DB struct {
	conn *pgx.ConnPool
}

func (db *DB) Close() {
	db.conn.Close()
}

func InitDBC(cfg *app.Config) (DB, error) {
	// const postgresURL = "postgres://postgres:pgAdmin@94.250.255.56:5432/botTgdb?sslmode=disable"
	pgxConfig := pgx.ConnConfig{
		Host:     cfg.Host,
		Database: cfg.Database,
		User:     cfg.User,
		Password: cfg.Password,
		Port:     uint16(cfg.Port),
		RuntimeParams: map[string]string{
			"standard_conforming_strings": "on",
			"client_encoding":             "UTF8",
		},
		PreferSimpleProtocol: true,
	}
	pgxConnPoolConfig := pgx.ConnPoolConfig{
		ConnConfig:     pgxConfig,
		MaxConnections: 3,
		AfterConnect:   nil,
		AcquireTimeout: 30,
	}

	conn, err := pgx.NewConnPool(pgxConnPoolConfig)

	if err != nil {
		return DB{conn: &pgx.ConnPool{}}, err
	}

	return DB{conn: conn}, nil
}

func (db DB) InitVariables(s *app.State) error {
	if err := db.conn.QueryRow(templateSql).Scan(&s.TemplateText, &s.TemplateFileID); err != nil {
		return fmt.Errorf("template init error: %w", err)
	}

	if err := db.conn.QueryRow(adviceSql).Scan(&s.AdviceText, &s.AdviceFileID); err != nil {
		return fmt.Errorf("advice init error: %w", err)
	}

	if err := db.conn.QueryRow(vipSql).Scan(&s.VipText, &s.VipFileID); err != nil {
		return fmt.Errorf("vip init error: %w", err)
	}

	if err := db.conn.QueryRow(dailySql).Scan(&s.DailyText, &s.DailyFileID); err != nil {
		return fmt.Errorf("daily init error: %w", err)
	}

	if err := db.conn.QueryRow(broadcastSql).Scan(&s.BroadcastText, &s.BroadcastFileID); err != nil {
		return fmt.Errorf("daily init error: %w", err)
	}
	return nil
}

func (db DB) UpdateChatID(chatID, userID int64) error {
	_, err := db.conn.Exec(chatIDSql, chatID, userID)
	return err
}

func (db DB) GetUsersAmount() (int64, error) {
	var usersTotal int64
	if err := db.conn.QueryRow(countSql).Scan(&usersTotal); err != nil {
		return 0, fmt.Errorf("getting users amount: %w", err)
	}
	return usersTotal, nil
}

func (db DB) UserGetOrAdd(userID int64, nickTG string) (app.RoleType, error) {
	var role string
	var nickDB string
	if err := db.conn.QueryRow(selectUserSql, userID).Scan(&role, &nickDB); err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			_, errAdd := db.conn.Exec(newUserSql, userID, 1, nickTG, time.Now())
			if errAdd != nil {
				return app.RoleUnknown, fmt.Errorf("Add user %w", errAdd)
			} else {
				return app.RoleUser, nil
			}
		}
		return app.RoleUnknown, fmt.Errorf("Get user %w", err)
	}
	if nickDB != nickTG {
		if _, err := db.conn.Exec(updateNickSql, userID, nickTG); err != nil {
			return app.RoleUnknown, fmt.Errorf("Update nickname user %w", err)
		}
	}
	return app.RoleType(role), nil
}

func (db DB) SetTemplate(t string) error {
	_, errAdd := db.conn.Exec(newTemplateSql, t, "", time.Now())
	if errAdd != nil {
		return fmt.Errorf("Set template %w", errAdd)
	}
	return nil
}

func (db DB) SetDaily(t string) error {
	_, errAdd := db.conn.Exec(newDailySql, t, "", time.Now())
	if errAdd != nil {
		return fmt.Errorf("Set daily %w", errAdd)
	}
	return nil
}

func (db DB) SetVip(t string) error {
	_, errAdd := db.conn.Exec(newVipSql, t, "", time.Now())
	if errAdd != nil {
		return fmt.Errorf("Set vip %w", errAdd)
	}
	return nil
}

func (db DB) SetAdvice(t string) error {
	_, errAdd := db.conn.Exec(newAdviceSql, t, "", time.Now())
	if errAdd != nil {
		return fmt.Errorf("Set advice %w", errAdd)
	}
	return nil
}

func (db DB) SetBroadcast(t string) error {
	_, errAdd := db.conn.Exec(newBroadcastSql, t, "", time.Now())
	if errAdd != nil {
		return fmt.Errorf("Set broadcast %w", errAdd)
	}
	return nil
}

func (db DB) GetChatIDs() (ids, chids []int64, names []string, err error) {
	rows, qErr := db.conn.Query(chatSql)
	if err != nil {
		err = qErr
		return
	}
	for rows.Next() {
		var chid int64
		var id int64
		var nm string
		if err := rows.Scan(&chid, &id, &nm); err != nil {
			continue
		}
		chids = append(chids, chid)
		ids = append(ids, id)
		names = append(names, nm)
	}
	rows.Close()
	return
}
