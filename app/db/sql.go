package db

var (
	templateSql  = `select text, img from templates order by timestamp desc limit 1`
	adviceSql    = `select text, img from advice order by timestamp desc limit 1`
	vipSql       = `select text, img from vip order by timestamp desc limit 1`
	dailySql     = `select text, img from daily order by timestamp desc limit 1`
	broadcastSql = `select text, img from broadcast order by timestamp desc limit 1`

	chatIDSql      = `update users set chat_id = $1 where telegram_id = int8($2)`
	countSql       = `SELECT COUNT ( telegram_id ) FROM users`
	selectUserSql  = `select role, telegram_user from roles, users where roles.id = "role_id" and "telegram_id" = $1`
	newUserSql     = `insert into users (telegram_id, role_id, telegram_user, date_time_added) values ($1, $2, $3, $4)`
	updateNickSql  = `update users set telegramuser = $2 where telegramid = $1`
	newTemplateSql = `insert into templates (text, img, timestamp) values ($1, $2, $3)`

	newDailySql     = `insert into daily (text, img, timestamp) values ($1, $2, $3)`
	newVipSql       = `insert into vip (text, img, timestamp) values ($1, $2, $3)`
	newAdviceSql    = `insert into advice (text, img, timestamp) values ($1, $2, $3)`
	newBroadcastSql = `insert into broadcast (text, img, timestamp) values ($1, $2, $3)`

	chatSql = `select chat_id, telegram_id, telegram_user from users`
)
