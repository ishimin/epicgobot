package db

import "epicbot/app"

type Conn interface {
	InitVariables(s *app.State) error
	UserGetOrAdd(userID int64, nickTG string) (app.RoleType, error)
	SetTemplate(t string) error
	SetDaily(t string) error
	SetVip(t string) error
	SetAdvice(t string) error
	SetBroadcast(t string) error
	UpdateChatID(chatID, userID int64) error
	GetUsersAmount() (int64, error)
	GetChatIDs() (ids, chids []int64, names []string, err error)
}
