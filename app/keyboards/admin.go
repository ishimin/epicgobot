package keyboards

import (
	"epicbot/app"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

var UserKeyboard = tgbotapi.NewInlineKeyboardMarkup(
	tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData("    Прогноз дня     ", app.UserDaily),
	),
	tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData("    VIP прогноз     ", app.UserVip),
	),
	tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData("    Надежная БК     ", app.UserAdvice),
	),
)

var AdminKeyboard = tgbotapi.NewInlineKeyboardMarkup(
	tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData("       Меню         ", app.AdminMenu),
	),
	tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData("    Прогноз дня     ", app.AdminDaily),
	),
	tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData("    VIP прогноз     ", app.AdminVip),
	),
	tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData("    Надежная БК     ", app.AdminAdvice),
	),
)

var AdminModeKeyboard = tgbotapi.NewInlineKeyboardMarkup(
	tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData("  Новый прогноз дня  ", app.AdminNewDaily),
	),
	tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData("  Новый VIP прогноз  ", app.AdminNewVip),
	),
	tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData("Изменить рекомендацию", app.AdminNewAdvice),
	),
	tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData(" Новый шаблон текста ", app.AdminNewTemplate),
	),
	tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData("Активные пользователи ", app.UsersTotal),
	),
	tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData("    Новая рассылка    ", app.AdminBroadcast),
	),
	tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData("      Разослать       ", app.AdminRunBroadcast),
	),
	tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData("        Выход         ", app.AdminExit),
	),
)

var RunBroadcastKeyboard = tgbotapi.NewInlineKeyboardMarkup(
	tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData("      Выполнить      ", app.AdminRunRunBroadcast),
	),
	tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData("        Выход         ", app.AdminExit),
	),
)
