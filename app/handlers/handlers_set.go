package handlers

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"

	"github.com/rs/zerolog/log"

	"epicbot/app"
	"epicbot/app/db"
	"epicbot/app/sender"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

func HandleSetTemplate(
	bot *tgbotapi.BotAPI,
	s *app.State,
	cfg app.Config,
	db db.Conn,
	adminMode bool,
	m *tgbotapi.Message,
	text string) {
	s.CurrentState = app.ReadOnly
	if m != nil {
		if m.Text != "" {
			s.TemplateText = m.Text
			if err := db.SetTemplate(m.Text); err != nil {
				log.Err(err).Msg("set template")
			}
		}
		if len(m.Photo) != 0 {
			s.TemplateFileID = m.Photo[len(m.Photo)-1].FileID
			f, _ := bot.GetFile(tgbotapi.FileConfig{FileID: s.TemplateFileID})
			s.TemplateFileURL = uploadFile(cfg, f.Link(bot.Token))
		}
	}
	sender.SendMessage(bot, s, db, false, m.Chat.ID, m.From.ID, m.From.UserName, "", true)
}

func HandleSetDaily(
	bot *tgbotapi.BotAPI,
	s *app.State,
	cfg app.Config,
	db db.Conn,
	adminMode bool,
	m *tgbotapi.Message,
	text string) {
	s.CurrentState = app.ReadOnly
	if m != nil {
		if m.Text != "" {
			s.DailyText = m.Text
			if err := db.SetDaily(m.Text); err != nil {
				log.Err(err).Msg("set daily")
			}
		}
		if len(m.Photo) != 0 {
			s.DailyFileID = m.Photo[len(m.Photo)-1].FileID
			f, _ := bot.GetFile(tgbotapi.FileConfig{FileID: s.DailyFileID})
			s.DailyFileURL = uploadFile(cfg, f.Link(bot.Token))
		}
	}
	sender.SendMessage(bot, s, db, false, m.Chat.ID, m.From.ID, m.From.UserName, "", true)
}

func HandleSetVip(
	bot *tgbotapi.BotAPI,
	s *app.State,
	cfg app.Config,
	db db.Conn,
	adminMode bool,
	m *tgbotapi.Message,
	text string) {
	s.CurrentState = app.ReadOnly
	if m != nil {
		if m.Text != "" {
			s.VipText = m.Text
			if err := db.SetVip(m.Text); err != nil {
				log.Err(err).Msg("set vip")
			}
		}
		if len(m.Photo) != 0 {
			s.VipFileID = m.Photo[len(m.Photo)-1].FileID
			f, _ := bot.GetFile(tgbotapi.FileConfig{FileID: s.VipFileID})
			s.VipFileURL = uploadFile(cfg, f.Link(bot.Token))
		}
	}
	sender.SendMessage(bot, s, db, false, m.Chat.ID, m.From.ID, m.From.UserName, "", true)
}

func HandleSetAdvice(
	bot *tgbotapi.BotAPI,
	s *app.State,
	cfg app.Config,
	db db.Conn,
	adminMode bool,
	m *tgbotapi.Message,
	text string) {
	s.CurrentState = app.ReadOnly
	if m != nil {
		if m.Text != "" {
			s.AdviceText = m.Text
			if err := db.SetAdvice(m.Text); err != nil {
				log.Err(err).Msg("set advice")
			}
		}
		if len(m.Photo) != 0 {
			s.AdviceFileID = m.Photo[len(m.Photo)-1].FileID
			f, _ := bot.GetFile(tgbotapi.FileConfig{FileID: s.AdviceFileID})
			s.AdviceFileURL = uploadFile(cfg, f.Link(bot.Token))
		}
	}
	sender.SendMessage(bot, s, db, false, m.Chat.ID, m.From.ID, m.From.UserName, "", true)
}

func HandleSetBroadcast(
	bot *tgbotapi.BotAPI,
	s *app.State,
	cfg app.Config,
	db db.Conn,
	adminMode bool,
	m *tgbotapi.Message,
	text string) {
	s.CurrentState = app.ReadOnly
	if m != nil {
		if m.Text != "" {
			s.BroadcastText = m.Text
			if err := db.SetBroadcast(m.Text); err != nil {
				log.Err(err).Msg("set broadcast")
			}
		}
		if len(m.Photo) != 0 {
			s.BroadcastFileID = m.Photo[len(m.Photo)-1].FileID
			f, _ := bot.GetFile(tgbotapi.FileConfig{FileID: s.BroadcastFileID})
			s.BroadcastFileURL = uploadFile(cfg, f.Link(bot.Token))
		}
	}
	sender.SendMessage(bot, s, db, false, m.Chat.ID, m.From.ID, m.From.UserName, "", true)
}

func uploadFile(cfg app.Config, photo string) string {
	data := url.Values{
		"image": {photo},
	}

	u := fmt.Sprintf("%s?expiration=%d&key=%s", cfg.CloudBaseURL, cfg.PhotoHostDuration, cfg.CloudKey)
	resp, err := http.PostForm(u, data)

	if err != nil {
		log.Err(err).Msg("upload file")
	}

	var res map[string]interface{}

	json.NewDecoder(resp.Body).Decode(&res)

	return fmt.Sprintf("%s", res["data"].(map[string]interface{})["display_url"])
}

func downloadFile(filepath string, url string) error {
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Create the file
	out, err := os.Create(filepath)
	if err != nil {
		return err
	}
	defer out.Close()

	// Write the body to file
	_, err = io.Copy(out, resp.Body)
	return err
}
