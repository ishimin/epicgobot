package handlers

import (
	"epicbot/app"
	"epicbot/app/db"
	"epicbot/app/sender"
	"unicode/utf8"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

func HandleUserDaily(bot *tgbotapi.BotAPI, s *app.State, cfg app.Config, db db.Conn, chatID, userID int64, userName string) {
	s.CurrentState = app.ReadOnly

	msg := tgbotapi.NewMessage(chatID, s.DailyText)
	if s.DailyFileID != "" {
		msg.Text = s.DailyText + "\x10\x10"
		msg.Entities = []tgbotapi.MessageEntity{{Type: "text_link", Offset: utf8.RuneCountInString(s.DailyText) - 1, Length: 2, URL: s.DailyFileURL}}
	}
	sender.SendMessageEntity(bot, s, db, false, chatID, userID, userName, msg, true)
}

func HandleUserVip(bot *tgbotapi.BotAPI, s *app.State, cfg app.Config, db db.Conn, chatID, userID int64, userName string) {
	s.CurrentState = app.ReadOnly
	msg := tgbotapi.NewMessage(chatID, s.VipText)
	if s.VipFileID != "" {
		msg.Text = s.VipText + "\x10\x10"
		msg.Entities = []tgbotapi.MessageEntity{{Type: "text_link", Offset: utf8.RuneCountInString(s.VipText) - 1, Length: 2, URL: s.VipFileURL}}
	}
	sender.SendMessageEntity(bot, s, db, false, chatID, userID, userName, msg, true)
}

func HandleUserAdvice(bot *tgbotapi.BotAPI, s *app.State, cfg app.Config, db db.Conn, chatID, userID int64, userName string) {
	s.CurrentState = app.ReadOnly
	msg := tgbotapi.NewMessage(chatID, s.AdviceText)
	if s.AdviceFileID != "" {
		msg.Text = s.AdviceText + "\x10\x10"
		msg.Entities = []tgbotapi.MessageEntity{{Type: "text_link", Offset: utf8.RuneCountInString(s.AdviceText) - 1, Length: 2, URL: s.AdviceFileURL}}
	}
	sender.SendMessageEntity(bot, s, db, false, chatID, userID, userName, msg, true)
}

func HandleAdminBroadcast(bot *tgbotapi.BotAPI, s *app.State, cfg app.Config, db db.Conn, chatID, userID int64, userName string) {
	s.CurrentState = app.RunBroadcast
	msg := tgbotapi.NewMessage(chatID, s.BroadcastText)
	if s.BroadcastFileID != "" {
		msg.Text = s.BroadcastText + "\x10\x10"
		msg.Entities = []tgbotapi.MessageEntity{{Type: "text_link", Offset: utf8.RuneCountInString(s.BroadcastText) - 1, Length: 2, URL: s.BroadcastFileURL}}
	}
	sender.SendMessageEntity(bot, s, db, false, chatID, userID, userName, msg, true)
}
