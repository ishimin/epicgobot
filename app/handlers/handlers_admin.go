package handlers

import (
	"fmt"

	"github.com/rs/zerolog/log"

	"epicbot/app"
	"epicbot/app/db"
	"epicbot/app/sender"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

func HandleAdminMenu(bot *tgbotapi.BotAPI, s *app.State, cfg app.Config, db db.Conn, chatID, userID int64, userName string) {
	s.CurrentState = app.ReadOnly
	sender.SendMessage(bot, s, db, true, chatID, userID, userName, "РЕЖИМ АДМИНИСТРАТОРА", true)
}

func HandleAdminDaily(bot *tgbotapi.BotAPI, s *app.State, cfg app.Config, db db.Conn, chatID, userID int64, userName string) {
	s.CurrentState = app.ReadOnly
	HandleUserDaily(bot, s, cfg, db, chatID, userID, userName)
}

func HandleAdminVip(bot *tgbotapi.BotAPI, s *app.State, cfg app.Config, db db.Conn, chatID, userID int64, userName string) {
	s.CurrentState = app.ReadOnly
	HandleUserVip(bot, s, cfg, db, chatID, userID, userName)
}

func HandleAdminAdvice(bot *tgbotapi.BotAPI, s *app.State, cfg app.Config, db db.Conn, chatID, userID int64, userName string) {
	s.CurrentState = app.ReadOnly
	HandleUserAdvice(bot, s, cfg, db, chatID, userID, userName)
}

func HandleNewAdminDaily(bot *tgbotapi.BotAPI, s *app.State, db db.Conn, chatID, userID int64, userName string) {
	s.CurrentState = app.SetDaily
	sender.SendMessage(bot, s, db, true, chatID, userID, userName, "ОТПРАВЬ ТЕКСТ ИЛИ КАРТИНКУ DAILY", false)
}

func HandleNewAdminVip(bot *tgbotapi.BotAPI, s *app.State, db db.Conn, chatID, userID int64, userName string) {
	s.CurrentState = app.SetVip
	sender.SendMessage(bot, s, db, true, chatID, userID, userName, "ОТПРАВЬ ТЕКСТ ИЛИ КАРТИНКУ VIP", false)
}

func HandleNewAdminAdvice(bot *tgbotapi.BotAPI, s *app.State, db db.Conn, chatID, userID int64, userName string) {
	s.CurrentState = app.SetAdvice
	sender.SendMessage(bot, s, db, true, chatID, userID, userName, "ОТПРАВЬ ТЕКСТ ПОСТА СОВЕТА", false)
}

func HandleNewAdminTemplate(bot *tgbotapi.BotAPI, s *app.State, db db.Conn, chatID, userID int64, userName string) {
	s.CurrentState = app.SetTemplate
	sender.SendMessage(bot, s, db, true, chatID, userID, userName, "ОТПРАВЬ ТЕКСТ ГЛАВНОГО ПОСТА", false)
}

func HandleNewAdminBroadcast(bot *tgbotapi.BotAPI, s *app.State, db db.Conn, chatID, userID int64, userName string) {
	s.CurrentState = app.SetBroadcast
	sender.SendMessage(bot, s, db, true, chatID, userID, userName, "ОТПРАВЬ ТЕКСТ РАССЫЛКИ", false)
}

func HandleRunAdminBroadcast(bot *tgbotapi.BotAPI, s *app.State, db db.Conn, chatID, userID int64, userName string) {
	s.CurrentState = app.ReadOnly
	sender.SendMessage(bot, s, db, false, chatID, userID, userName, "Рассылка запущена", true)
	chids, ids, names, err := db.GetChatIDs()
	if err != nil {
		sender.SendMessage(bot, s, db, false, chatID, userID, userName, "Ошибка получения пользователей из БД", true)
	}

	go func(chids []int64, ids []int64, names []string) {
		for i := 0; i < len(ids); i++ {
			msg := tgbotapi.NewMessage(chids[i], s.BroadcastText)
			if s.BroadcastFileID != "" {
				msg.Text = s.BroadcastText + "\x10\x10"
				msg.Entities = []tgbotapi.MessageEntity{{Type: "text_link", Offset: len([]rune(s.BroadcastText)) - 1, Length: 2, URL: s.BroadcastFileURL}}
			}
			if chids[i] != 0 {
				sender.SendMessageEntity(bot, s, db, false, chids[i], ids[i], names[i], msg, true)
				log.Debug().
					Int64("chatID", chids[i]).
					Int64("userID", ids[i]).
					Str("name", names[i]).
					Msg("broadcast message")
			}
		}
	}(chids, ids, names)
}

func HandleUsersTotal(bot *tgbotapi.BotAPI, s *app.State, db db.Conn, chatID, userID int64, userName string) {
	s.CurrentState = app.ReadOnly
	usersTotal, err := db.GetUsersAmount()
	if err != nil {
		log.Err(err).Msg("get users amount")
	}
	sender.SendMessage(bot, s, db, false, chatID, userID, userName, fmt.Sprintf("Users: %d", usersTotal), true)
}

func HandleAdminExit(bot *tgbotapi.BotAPI, s *app.State, db db.Conn, chatID, userID int64, userName string) {
	s.CurrentState = app.ReadOnly
	sender.SendMessage(bot, s, db, false, chatID, userID, userName, "", true)
}
