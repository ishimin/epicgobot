package app

type StateType string

const (
	ReadOnly     StateType = "read_only"
	SetDaily     StateType = "set_daily"
	SetVip       StateType = "set_vip"
	SetAdvice    StateType = "set_advice"
	SetTemplate  StateType = "set_template"
	SetBroadcast StateType = "set_broadcast"
	RunBroadcast StateType = "run_broadcast"
)

type State struct {
	CurrentState     StateType
	DailyText        string
	DailyFileID      string
	DailyFileURL     string
	VipText          string
	VipFileID        string
	VipFileURL       string
	AdviceText       string
	AdviceFileID     string
	AdviceFileURL    string
	TemplateText     string
	TemplateFileID   string
	TemplateFileURL  string
	BroadcastText    string
	BroadcastFileID  string
	BroadcastFileURL string
}
