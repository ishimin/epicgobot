package app

type RoleType string

const (
	RoleUnknown RoleType = "unknown"
	RoleUser    RoleType = "user"
	RoleAdmin   RoleType = "admin"
)

const (
	UserDaily            string = "daily"
	UserVip              string = "vip"
	UserAdvice           string = "advice"
	AdminMenu            string = "admin_menu"
	AdminDaily           string = "admin_daily"
	AdminVip             string = "admin_vip"
	AdminAdvice          string = "admin_advice"
	AdminNewDaily        string = "admin_new_daily"
	AdminNewVip          string = "admin_new_vip"
	AdminNewAdvice       string = "admin_new_advice"
	AdminNewTemplate     string = "admin_new_template"
	UsersTotal           string = "admin_users_total"
	AdminBroadcast       string = "admin_broadcast"
	AdminRunBroadcast    string = "admin_run_broadcast"
	AdminRunRunBroadcast string = "admin_run_run_broadcast"
	AdminExit            string = "admin_exit"
)
