package main

import (
	"os"
	"os/signal"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"

	"epicbot/app"
	"epicbot/app/db"
	"epicbot/app/handlers"
	"epicbot/app/sender"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

func main() {
	var s app.State
	s.CurrentState = app.ReadOnly

	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	zerolog.SetGlobalLevel(zerolog.InfoLevel)

	var cfg app.Config
	cfg.LoadDefaults()

	dbConn, err := db.InitDBC(&cfg)
	if err != nil {
		log.Panic().Err(err).Msg("db connect failed")
	}
	defer dbConn.Close()

	if err = dbConn.InitVariables(&s); err != nil {
		log.Err(err).Msg("init variables failed")
	}

	go ServeBot(cfg, &s, dbConn)

	waitForInterrupt()
}

func ServeBot(cfg app.Config, s *app.State, conn db.Conn) {
	bot, err := tgbotapi.NewBotAPI(cfg.TelegramToken)
	if err != nil {
		log.Panic().Err(err).Msg("bot API client connection failed")
	}
	log.Info().Str("Account", bot.Self.LastName).Msg("Authorized")

	bot.Debug = cfg.BotDebug
	updatesConf := tgbotapi.NewUpdate(0)
	updatesConf.Timeout = cfg.BotTimeout

	updatesChan := bot.GetUpdatesChan(updatesConf)

	for update := range updatesChan {
		if update.Message != nil {
			processMessage(bot, s, cfg, conn, update.Message)
		} else if q := update.CallbackQuery; q != nil {
			processCallback(bot, s, cfg, conn, q)
		}
	}
}

func processMessage(bot *tgbotapi.BotAPI, s *app.State, cfg app.Config, db db.Conn, m *tgbotapi.Message) {
	log.Debug().Msgf("received: %+v", m)
	switch s.CurrentState {
	case app.SetTemplate:
		handlers.HandleSetTemplate(bot, s, cfg, db, true, m, "")
	case app.SetDaily:
		handlers.HandleSetDaily(bot, s, cfg, db, true, m, "")
	case app.SetVip:
		handlers.HandleSetVip(bot, s, cfg, db, true, m, "")
	case app.SetAdvice:
		handlers.HandleSetAdvice(bot, s, cfg, db, true, m, "")
	case app.SetBroadcast:
		handlers.HandleSetBroadcast(bot, s, cfg, db, true, m, "")
	default:
		sender.SendMessage(bot, s, db, false, m.Chat.ID, m.From.ID, m.From.UserName, "", true)
	}
}

func processCallback(bot *tgbotapi.BotAPI, s *app.State, cfg app.Config, db db.Conn, q *tgbotapi.CallbackQuery) {
	switch q.Data {
	case app.UserDaily:
		handlers.HandleUserDaily(bot, s, cfg, db, q.Message.Chat.ID, q.From.ID, q.From.UserName)
		if err := db.UpdateChatID(q.Message.Chat.ID, q.From.ID); err != nil {
			log.Err(err).
				Int64("chatID", q.Message.Chat.ID).
				Int64("userID", q.From.ID).
				Msg("update chat id")
		}
	case app.UserVip:
		handlers.HandleUserVip(bot, s, cfg, db, q.Message.Chat.ID, q.From.ID, q.From.UserName)
	case app.UserAdvice:
		handlers.HandleUserAdvice(bot, s, cfg, db, q.Message.Chat.ID, q.From.ID, q.From.UserName)
	case app.AdminMenu:
		handlers.HandleAdminMenu(bot, s, cfg, db, q.Message.Chat.ID, q.From.ID, q.From.UserName)
	case app.AdminDaily:
		handlers.HandleAdminDaily(bot, s, cfg, db, q.Message.Chat.ID, q.From.ID, q.From.UserName)
	case app.AdminVip:
		handlers.HandleAdminVip(bot, s, cfg, db, q.Message.Chat.ID, q.From.ID, q.From.UserName)
	case app.AdminAdvice:
		handlers.HandleAdminAdvice(bot, s, cfg, db, q.Message.Chat.ID, q.From.ID, q.From.UserName)
	case app.AdminNewDaily:
		handlers.HandleNewAdminDaily(bot, s, db, q.Message.Chat.ID, q.From.ID, q.From.UserName)
	case app.AdminNewVip:
		handlers.HandleNewAdminVip(bot, s, db, q.Message.Chat.ID, q.From.ID, q.From.UserName)
	case app.AdminNewAdvice:
		handlers.HandleNewAdminAdvice(bot, s, db, q.Message.Chat.ID, q.From.ID, q.From.UserName)
	case app.AdminNewTemplate:
		handlers.HandleNewAdminTemplate(bot, s, db, q.Message.Chat.ID, q.From.ID, q.From.UserName)
	case app.AdminBroadcast:
		handlers.HandleNewAdminBroadcast(bot, s, db, q.Message.Chat.ID, q.From.ID, q.From.UserName)
	case app.AdminRunBroadcast:
		handlers.HandleAdminBroadcast(bot, s, cfg, db, q.Message.Chat.ID, q.From.ID, q.From.UserName)
	case app.AdminRunRunBroadcast:
		handlers.HandleRunAdminBroadcast(bot, s, db, q.Message.Chat.ID, q.From.ID, q.From.UserName)
	case app.UsersTotal:
		handlers.HandleUsersTotal(bot, s, db, q.Message.Chat.ID, q.From.ID, q.From.UserName)
	case app.AdminExit:
		handlers.HandleAdminExit(bot, s, db, q.Message.Chat.ID, q.From.ID, q.From.UserName)
	default:
	}
	sender.SendPopup(bot, q.ID, q.From.UserName, "") //"User id: "+strconv.FormatInt(q.From.ID, 10))
}

func waitForInterrupt() {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c
}
